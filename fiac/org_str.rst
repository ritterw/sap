SAP FI – 企业结构 Organization Structure
==========================================

对企业整体一个相关的设置.

定义公司 Define Company
---------------------------------------

``T-CODE: OX15``

定义公司代码 Define Company Code
---------------------------------------

``T-CODE: OX02``

将公司代码分配给公司 Assign Company Code to Company
----------------------------------------------------

``T-CODE: OX16``

定义业务范围 Define Business Area
---------------------------------------

``T-CODE: OX03``

定义功能范围 Define Functional Area
---------------------------------------

``T-CODE: OKBD``

定义信贷控制 Define Credit Control
---------------------------------------

这块控制客户信用,并需要与SD模块对接.

``T-CODE: OB45``

给信贷控制区分配公司代码 Assign Company Code to Credit Control
-------------------------------------------------------------

``T-CODE: spro``
  企业结构 -> 分配 -> 财务会计 -> 给信贷控制区分配公司代码



