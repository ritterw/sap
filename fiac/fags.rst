SAP FI – 财务会计全局设置 Financial Accounting Global Settings
===============================================================

对财务会计全局的设置.


维护会计年度变式 Maintain Fiscal Year Variant
---------------------------------------------------------------

``T-CODE: OB29``

中国默认设置为 ``K4`` .


将公司代码分配给会计年度变式 Assign Company Code to Fiscal Year Variant
-------------------------------------------------------------------------------

``T-CODE: OB37``

定义未结清过帐期间变式 Define variants for Open Posting Periods
---------------------------------------------------------------

``T-CODE: OBBO``

未清和关帐过帐期间 Open and Close Posting Periods
---------------------------------------------------------------

``T-CODE: OB52``


将变式分配给公司代码 Assign Posting Period Variant to Company Code
--------------------------------------------------------------------------------

``T-CODE: OBBP``


定义字段状态变式 Define Field Status Variants
---------------------------------------------------------------

``T-CODE: OBC4``

copy 1000 to 0008 :全球任意通通讯公司 字段状态变式

| FStGroup | Text |
G001	General (with text, assignment)

G003	Material consumption accounts

G004	Cost accounts

G005	Bank accounts (obligatory value date)

G006	Material accounts

G007	Asset accts (w/o accumulated depreciation)

G008	Assets area clearing accounts

G009	Bank accounts (obligatory due date)

G011	Clearing accounts (with settlement per.)

G012	Receivables/payables clearing

G013	General (obligatory text)

G014	MM adjustment accounts

G017	Freight/customs provisions/clearing (MM)

G018	Scrapping (MM)

G019	Other Receivables/Payables

G023	Plant maintenance accounts

G025	Inventory adjustment accounts

G026	Accounts for down payments made

G029	Revenue accounts

G030	Change in stock accounts

G031	Accounts for down payments received

G032	Bank accounts (obligat. value/due dates)

G033	Cost accounts for external services

G036	Revenue accts (with cost center, PA)

G039	Accts for pmnts on acct made for assets

G040	Personnel clearing accounts

G041	Tax office clearing accounts

G045	Goods/invoice received clearing accounts

G049	Manufacturing costs accounts

G050	Central control clearing accounts

G052	Accounts for fixed asset retirement

G056	Amortization accounts

G059	Inventory accounting material stock acct

G062	Investment support accounts

G064	Other cost accounts (obligatory text)

G067	Reconciliation accounts

G068	Reconciliation accts (payables – Austria)

G069	Cost accounts (travel expenses)

G070	Clearing accounts (travel expenses)

G071	Reconciliation accts ( KIDNO /Foreign payment)



将公司代码分配给字段状态变式 Assign Company Code to Field Status Variants
----------------------------------------------------------------------------

``T-CODE: OBC5``

将1008分配给公司.



定义过帐码 Define Posting Keys
---------------------------------------------------------------

``T-CODE: 0B41``


定义凭证类型 Define Document Types
---------------------------------------------------------------

``T-CODE: OBA7``



定义凭证号码段 Define Document Number Ranges
---------------------------------------------------------------

``T-CODE: FBN1``

这块可以使用 复制凭证号码段 


定义雇员的容差组 Define Tolerance Group for Employees
---------------------------------------------------------------

``T-CODE: OBA4``

容差组:0008 公司代码:0008

注意: 在容差组这个空中要空着.不需要填什么说明.



为主帐科目定义容差组 Define Tolerance Group for G/L Accounts
---------------------------------------------------------------

``T-CODE: oBA0``

容差组:0008 公司代码:0008

将用户分配到容差组 Assign Users to Tolerance Group
---------------------------------------------------------------

``T-CODE: OB57``

OBA3 定义供应商/客户 容差组.


公司代码全局性的参数 Global Parameters for Company Code
---------------------------------------------------------------

``T-CODE: OBY6``



