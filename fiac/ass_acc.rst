SAP FI –固定资产 Asset Accounting
======================================================================

Chart of Depreciation
----------------------------------------------------------------------
``T-CODE: 0B41``

复制 1000 到自己




Assign Chart of Depreciation to Company Code
----------------------------------------------------------------------

``T-CODE: 0B41``





Specify Account Determination
----------------------------------------------------------------------

``T-CODE: 0B41``





Create Screen Layout Rules
----------------------------------------------------------------------

``T-CODE: 0B41``





Maintain asset number ranges
----------------------------------------------------------------------

``T-CODE: 0B41``





Define Asset Classes
----------------------------------------------------------------------

``T-CODE: 0B41``





Determine Depreciation area in the Asset Class
----------------------------------------------------------------------
``T-CODE: 0B41``






Assignment of General Ledger Accounts
----------------------------------------------------------------------

``T-CODE: 0B41``





Define Screen Layout for Asset master data
----------------------------------------------------------------------

``T-CODE: 0B41``





Define Screen Layout for Asset Depreciation Areas
----------------------------------------------------------------------

``T-CODE: 0B41``





Maintain Depreciation key
----------------------------------------------------------------------

``T-CODE: 0B41``




