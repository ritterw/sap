.. SAP 一步步入门 documentation master file, created by
   sphinx-quickstart on Fri Dec  7 13:54:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SAP 一步步入门
===================

.. image:: images/saplogo.png
   :align: left
   :width: 454px
   :height: 151px
   :scale: 55%


:Authors:
    Richard.Wang

:Version: 1.1 of 2014/06/20

:Dedication: To my Grandmother and my wife.


:WebSite: www.sap.com

很多人都感觉 ``SAP`` 这个东西很难入门.我也有同感,但是我们反回来想想,它只是一个应用软件,就像我们常用的
``Office`` 软件一样,有什么可难的呢? 可是,实际不是这样的,为什么会这样呢?

#. ``SAP`` 是专业领域使用的应用软件,所以,需要我们去懂相关的专业领域的知识.

#. 需要去解理软件设计的理论,将专业的领域业务用软件来实现.

根据上面的两点, 一步步记录在学习 ``SAP`` 过程中, 所遇的问题和对专业知识的理解. 我也是刚刚开始学习, 对于
财务知识的匮乏, 在这个过程中可能会对一些知道点的理解错误, 请大家指正. ritterw AT 126 刀塔 com .


SAP - FICO
-----------------------------

财务会计模块,下面是将FI和CO模块分别再详细的分为几个部分. 需要先对这个几部分进行配置.

.. toctree::
   :maxdepth: 2

   fiac/org_str
   fiac/fags
   fiac/gen_led
   fiac/acc_pay
   fiac/acc_rec
   fiac/bank_acc
   fiac/ass_acc
   fiac/note
   sap_co/cost_ca
   sap_co/pro_cost
   sap_co/cost_elacc
   sap_co/pro_cer

   sap_co/quest



SAP - BI
-----------------------------

商务智能, 数据分析.

.. toctree::
   :maxdepth: 2

   company/company

青岛星海集团
-----------------------------

青岛星海集团项目,详细的实施的步骤.

.. toctree::
   :maxdepth: 2

   xinghai/base

实现 ``SAP`` ``FICO`` 模块化基本标配


.. toctree::
   :maxdepth: 2

   xinghai/config

以拍拍熊易网公司为案例来,实现 ``SAP`` ``FICO`` 模块化基本标配.


.. toctree::
   :maxdepth: 2

   xinghai/bear

山河瑞杰集团
-----------------------------

山河项目,详细的实施的步骤.主要有两个分公司,一个山河北京, 一个山河上海.
首来配置 ``FI`` 模块.

.. toctree::
   :maxdepth: 2

   mr/fi_base

实现 ``SAP`` ``CO`` 模块化基本标配


.. toctree::
   :maxdepth: 2

   mr/co_base




附录:
-----------------------------

.. toctree::
   :maxdepth: 2
 
   refer/word
   refer/ascii

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
.. tips warning and note.

.. warning::

   **声明:**

   这是私人的文档,不能与别人共享.  本文档由 Richard.Wang 撰写，保留所有权利.

   `知识共享许可协议 <http://creativecommons.org/licenses/by-nc-nd/3.0/cn/legalcode>`_
   `<<SAP 一步步入门>> <http://sap.readthedocs.org/>`_ 由 \
   `Richard.Wang <http://sap.readthedocs.org/en/latest>`_  创作.

   采用 \ `署名-非商业性使用-禁止演绎 3.0 中国大陆 (CC BY-NC-ND 3.0 CN)
   <http://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>`_  进行许可。

   .. image:: images/license.png
..     :align: center
