SAP CO - 成本中心会计 Cost center Accounting
==================================================================



Maintain Controlling Area
-------------------------------------------------------------------

T-code: OKKP





Assign company code to controlling area
-------------------------------------------------------------------






Maintain Number Ranges for Controlling Areas
-------------------------------------------------------------------






Maintain versions in SAP Controlling
-------------------------------------------------------------------






Activate Components
-------------------------------------------------------------------






Maintain currency and valuation profile
-------------------------------------------------------------------





Assign Maintain currency and valuation profile to controlling area
-------------------------------------------------------------------





Create Primary Cost Elements
-------------------------------------------------------------------





Automatic creation of primary cost elements
-------------------------------------------------------------------





Create Secondary Cost Elements
-------------------------------------------------------------------





Create cost element group
-------------------------------------------------------------------





Create Cost Center
-------------------------------------------------------------------





Create Cost Center Group
-------------------------------------------------------------------





Cost Center hierarchy
-------------------------------------------------------------------





Create assessment cost elements
-------------------------------------------------------------------





Define Cost center categories
-------------------------------------------------------------------





